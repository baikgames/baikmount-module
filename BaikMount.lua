local BaikCore = LibStub("AceAddon-3.0"):GetAddon("BaikCore")
local Assert = BaikCore.Assert
local ChatCommand = BaikCore.ChatCommand
local Log = BaikCore.Log

-- Add Module to BaikCore
local BaikMount = BaikCore:NewModule("BaikMount")
BaikCore.BaikMount = BaikMount

-- Constants
local RESTRICTED_MAPS = {
    "Vashjir",
    "AhnQiraj"
}

local RANDOM_MOUNT_ID = 0

-- Private Methods
local function SummonMount()
    local state = BaikMount.State
    local mountId = BaikMount.FavoriteMount:GetRandomMount()

    -- Check for valid conditions otherwise use blizzard random favorite mount
    if IsSwimming() or IsFlyableArea() or not state:IsSummonable(mountId) or
        state:ContainsMap(RESTRICTED_MAPS) then
        mountId = RANDOM_MOUNT_ID
        Log:d("BaikMount_SummonMount: Fallback to blizzard random mount")
    end

    C_MountJournal.SummonByID(mountId)
end

-- Ace3 Callbacks
function BaikMount:OnInitialize()
    Log:v("BaikMount_OnInitialize")
end

function BaikMount:OnEnable()
    ChatCommand:Register("mount", SummonMount, "Summons a random favorite mount")
    Log:v("BaikMount_OnEnable")
end

function BaikMount:OnDisable()
    ChatCommand:Deregister("mount")
    Log:v("BaikMount_OnDisable")
end
