local BaikCore = LibStub("AceAddon-3.0"):GetAddon("BaikCore")
local BaikMount = BaikCore.BaikMount
local Assert = BaikCore.Assert
local Log = BaikCore.Log
local Table = BaikCore.Table

-- Add Module to BaikMount
local FavoriteMount = BaikMount:NewModule("FavoriteMount", "AceEvent-3.0")
BaikMount.FavoriteMount = FavoriteMount

-- Private Variables
local favorites = {}

-- Private Methods
local function IsFavorite(id)
    Assert:Number(id)
    return select(7, C_MountJournal.GetMountInfoByID(id))
end

local function GetAllFavoriteMounts()
    local mounts = {}
    local mountIds = C_MountJournal:GetMountIDs()
    for idx, id in pairs(mountIds) do
        if idx and IsFavorite(id) then
            table.insert(mounts, id)
        end
    end
    Log:i("Total mounts %d", Table:Size(mounts))
    return mounts
end

-- Public API
function FavoriteMount:MOUNT_JOURNAL_SEARCH_UPDATED()
    favorites = GetAllFavoriteMounts()
end

function FavoriteMount:GetRandomMount()
    return #favorites > 0 and favorites[math.random(#favorites)] or 0
end

-- Ace3 Callbacks
function FavoriteMount:OnInitialize()
    Log:v("FavoriteMount_OnInitialize")
end

function FavoriteMount:OnEnable()
    self:RegisterEvent("MOUNT_JOURNAL_SEARCH_UPDATED")
    favorites = GetAllFavoriteMounts()
    Log:v("FavoriteMount_OnEnable")
end

function FavoriteMount:OnDisable()
    self:UnregisterEvent("MOUNT_JOURNAL_SEARCH_UPDATED")
    favorites = {}
    Log:v("FavoriteMount_OnDisable")
end
