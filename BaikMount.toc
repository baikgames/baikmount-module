## Interface: 80000
## Version: 8.0.1
## Title: BaikMount
## Author: Nikpmup
## Notes: Provides better random mount selection
## Dependencies: BaikCore
## OptionalDeps: Ace3
## X-Embeds: Ace3
embeds.xml
BaikMount.lua
Util/State.lua
Module/FavoriteMount.lua
