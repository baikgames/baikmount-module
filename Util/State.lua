local BaikCore = LibStub("AceAddon-3.0"):GetAddon("BaikCore")
local BaikMount = BaikCore:GetModule("BaikMount")
local Assert = BaikCore.Assert

--Add Module
local State = {}
BaikMount.State = State

-- Public API
function State:IsSummonable(mountId)
    Assert:Number(mountId)
    return select(5, C_MountJournal.GetMountInfoByID(mountId))
end

function State:IsMap(mapName)
    Assert:String(mapName)
    local curMap = select(1, GetMapInfo())

    return curMap and curMap:lower():find(mapcurMap:lower()) or false
end

function State:ContainsMap(mapTable)
    Assert:Table(mapTable)
    -- Get current map
    local mapId = C_Map.GetBestMapForUnit("player")
    local curMap = C_Map.GetMapInfo(mapId).name
    if not curMap then
        return false
    end

    -- Check if map exists in table
    for key, mapName in pairs(mapTable) do
        if curMap:lower():find(mapName:lower()) then
            return true
        end
    end

    return false
end
